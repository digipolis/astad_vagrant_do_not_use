# Vagrant setup #

This setup has been created via https://puphpet.com/

Changes can be done by dragging the config.yaml file to the puphpet website.

## TODO: ##

* Auto configuration of zend apps
* install nodeman via install script
* start up nodeApps automagically
* Load fixtures automagically
* Script to do stuff of getting started
* Make in Windows Compabibel ( NFS not possible on windows)

## Getting started ##

* Create a directory where you will put all repository

```
    mkdir Digipolis

    cd Digipolis

    mkdir node_apps

    git clone git@bitbucket.org:peetmar/astad_vagrant.git

    git clone git@bitbucket.org:digipolis/astad_php_zend_app_www.git

    git clone git@bitbucket.org:digipolis/astad_php_psr_composer_astad.git

    cd node_apps

    # Clone all the node apps to the folder node_apps

    git clone git@bitbucket.org:astad_nodejs_expressjs_app_kanalen.git

```

* Manually set your config.php.dist files
* Start up vagrant


```
    cd Digipolis\astad_vagrant

    vagrant up

```
First time vagrant will install a virtual box and share following directories to your vm

     Host                                       | VM
    :-------------------------------------------|------------------------------------------:
     Digipolis/astad_php_zend_app_www/          | /var/www/ds.dev/
     Digipolis/astad_php_psr_composer_astad/    | /var/www/astad_php_psr_composer_astad/
     Digipolis/node_apps                         | /var/node_apps/


* After first install  nodemon
```
    npm install -g nodemon
```

* Add this to you host file

```
    192.168.56.101  ds.dev

    192.168.56.101  www.ds.dev
```

Login in your box

```
vagrant ssh
```

And run composer

```
cd /var/www/astad_php_zend_app_www

composer install
```

* www.ds.dev should give you the website.
